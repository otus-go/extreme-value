package max

import (
	"fmt"
	"strconv"
	"testing"
)

func TestMaxEmpty(t *testing.T) {
	s := []interface{}{}
	cmp := func(a, b interface{}) (bool, error) {
		return true, nil
	}
	res, _ := Extreme(s, cmp)
	if res != nil {
		t.Error("Not nil result from empty slice")
	}
}

func TestMaxIntSingleElement(t *testing.T) {
	s := []interface{}{100500}
	c := func(a, b interface{}) (bool, error) {
		return false, nil
	}
	res, _ := Extreme(s, c)
	if res != 100500 {
		t.Errorf("Got %d, expected 100500", res)
	}
}

func TestMaxInt(t *testing.T) {
	s := []interface{}{2, 12, 85, 0, 6}
	cmp := func(a, b interface{}) (bool, error) {
		intA, _ := a.(int)
		intB, _ := b.(int)
		return intA > intB, nil
	}
	res, _ := Extreme(s, cmp)
	if res != 85 {
		t.Errorf("Got %d, expected 85", res)
	}
}

func TestMinInt(t *testing.T) {
	s := []interface{}{2, 12, 85, 0, 6}
	cmp := func(a, b interface{}) (bool, error) {
		intA, _ := a.(int)
		intB, _ := b.(int)
		return intA < intB, nil
	}
	res, _ := Extreme(s, cmp)
	if res != 0 {
		t.Errorf("Got %d, expected 0", res)
	}
}

func TestMaxString(t *testing.T) {
	s := []interface{}{"2", "12", "85", "9", "0", "6"}
	c := func(a, b interface{}) (bool, error) {
		strA, _ := a.(string)
		strB, _ := b.(string)
		return strA > strB, nil
	}
	res, _ := Extreme(s, c)
	if res != "9" {
		t.Errorf("Got %s, expected \"9\"", res)
	}
}

func TestMaxMixed(t *testing.T) {
	s := []interface{}{"2", 12, "85", 9.5, "0", "6"}

	convert := func(x interface{}) (float64, error) {
		switch v := x.(type) {
		case int:
			return float64(v), nil
		case float64:
			return v, nil
		case string:
			res, err := strconv.ParseFloat(v, 64)
			if err != nil {
				return 0.0, err
			}
			return res, nil
		default:
			return 0.0, fmt.Errorf("uncomparable type")
		}
	}

	cmp := func(a, b interface{}) (bool, error) {
		floatA, err1 := convert(a)
		floatB, err2 := convert(b)
		if err1 != nil {
			return false, err1
		}
		if err2 != nil {
			return false, err2
		}
		return floatA > floatB, nil
	}
	res, _ := Extreme(s, cmp)
	if res != "85" {
		t.Errorf("Got %s, expected \"85\"", res)
	}
}

func TestMaxError(t *testing.T) {
	s := []interface{}{1, 2}
	cmp := func(a, b interface{}) (bool, error) {
		return false, fmt.Errorf("uncomparable type")
	}
	_, err := Extreme(s, cmp)
	if err == nil {
		t.Error("Not nil error expected")
	}
}
