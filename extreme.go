package max

// Comparator take two interfaces and
// returns true if first argument is greater than
// second one. Return error if type not supported
type Comparator func(a, b interface{}) (bool, error)

// Extreme take slice of interfaces and user-defined
// comparator and returns max element from slice
// Return nil if empty slice passed
func Extreme(items []interface{}, cmp Comparator) (interface{}, error) {
	var extrema interface{}
	for i, item := range items {
		if i == 0 { // on first iteration just init value
			extrema = item
			continue
		}
		var (
			updateExtrema bool
			err           error
		)
		if updateExtrema, err = cmp(item, extrema); err != nil {
			return false, err
		}
		if updateExtrema {
			extrema = item
		}
	}
	return extrema, nil
}
